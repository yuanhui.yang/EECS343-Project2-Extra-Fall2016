// shmem_access

#include "types.h"
#include "user.h"
#include "stat.h" 
#include "param.h"

int
main(int argc, char **argv)
{
  if(argc < 1){
    printf(2, "usage: shmem_access page_number\n");
    exit();
  }
  printf(1, "%d\n", (int)shmem_access(atoi(argv[1])));
  exit();
}
