// shmem_count

#include "types.h"
#include "user.h"
#include "stat.h" 
#include "param.h"

int
main(int argc, char **argv)
{
  if(argc < 1){
    printf(2, "usage: shmem_count page_number\n");
    exit();
  }
  printf(1, "%d", shmem_count(atoi(argv[1])));
  exit();
}
